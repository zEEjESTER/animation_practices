﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public Animator animator;
    public bool isSprinting;
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        Move();
    }

    private void Move()
    {
        animator.SetInteger("xAxisRaw", Mathf.RoundToInt(Input.GetAxisRaw("Vertical")));
        animator.SetFloat("xAxis", Input.GetAxisRaw("Vertical"));
        this.transform.Rotate(Vector3.up * Input.GetAxis("Horizontal"));

        if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetAxis("Axis 9") > 0f)
        {
            isSprinting = true;
            animator.SetBool("isSprinting", isSprinting);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift) || (!Input.GetKey(KeyCode.LeftShift) && Input.GetAxis("Axis 9") == 0f))
        {
            isSprinting = false;
            animator.SetBool("isSprinting", isSprinting);

        }
    }


}
